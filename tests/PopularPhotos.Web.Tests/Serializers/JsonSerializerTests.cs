﻿using PopularPhotos.Web.Serializers;
using System;
using System.Collections.Generic;
using Xunit;

namespace PopularPhotos.Web.Tests.Serializers
{
  public class JsonSerializerTests
  {
    private readonly IJsonSerializer jsonSerializer;

    public JsonSerializerTests()
    {
      jsonSerializer = new JsonSerializer();
    }

    public static IEnumerable<object[]> GetFakeEntities()
    {
      yield return new object[] { new FakeEntity() { Id = 1 }, "{\"Id\":1}" };
    }

    public class DeserializeMethod : JsonSerializerTests
    {
      [Fact]
      public void ShouldThrowArgumentNullException()
      {
        // Assert
        Assert.Throws<ArgumentNullException>(() => jsonSerializer.Deserialize<FakeEntity>(null));
      }

      [Theory]
      [MemberData(nameof(GetFakeEntities))]
      public void ShouldDeserializeToFakeEntity(
        FakeEntity fakeEntity,
        string json)
      {
        // Assert
        var result = Assert.IsType<FakeEntity>(jsonSerializer.Deserialize<FakeEntity>(json));
        Assert.Equal(fakeEntity.Id, result.Id);
      }
    }

    public class SerializeMethod : JsonSerializerTests
    {
      
      [Fact]
      public void ShouldThrowArgumentNullException()
      {
        //Assert
        Assert.Throws<ArgumentNullException>(() => jsonSerializer.Serialize(null));
      }


      [Theory]
      [MemberData(nameof(GetFakeEntities))]
      public void TestName(
        FakeEntity fakeEntity,
        string json)
      {
        // Assert
        var result = Assert.IsType<string>(jsonSerializer.Serialize(fakeEntity));
        Assert.Equal(json, result);
      }
    }

    public class FakeEntity
    {
      public int Id { get; set; }
    }

  }
}
