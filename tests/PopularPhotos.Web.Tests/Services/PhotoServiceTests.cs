﻿using Moq;
using PopularPhotos.Web.Clients;
using PopularPhotos.Web.Models;
using PopularPhotos.Web.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace PopularPhotos.Web.Tests.Services
{
  public class PhotoServiceTests
  {
    public class PageAsyncMethod : PhotoServiceTests
    {
      public static IEnumerable<object[]> GetPageAsyncData()
      {
        yield return new object[] { "popular", new PhotosPagedResponse() { feature = "popular", current_page = 1 } };        
      }

      [Theory]
      [MemberData(nameof(GetPageAsyncData))]
      public async Task ShouldReturnValidPhotosPagedResponse(
        string feature,
        PhotosPagedResponse photosPagedResponse)
      {
        // Arrange
        var jsonClient = new Mock<IJsonClient>();

        jsonClient
          .Setup(c => c.GetAsync<PhotosPagedResponse>(It.IsAny<Uri>()))
          .ReturnsAsync(photosPagedResponse);

        var photoService = new PhotoService(jsonClient.Object, new PhotosApiSettings());

        // Act
        var result = await photoService.PageAsync(feature, 1, "rating", "asc");

        // Assert
        Assert.Equal(feature, result.feature);
      }

      [Fact]
      public async Task ShouldReturnEmptyPhotosPagedResponse()
      {
        // Arrange
        var jsonClient = new Mock<IJsonClient>();

        jsonClient
          .Setup(c => c.GetAsync<PhotosPagedResponse>(It.IsAny<Uri>()))
          .ReturnsAsync(default(PhotosPagedResponse));

        var photoService = new PhotoService(jsonClient.Object, new PhotosApiSettings());

        // Act
        var result = await photoService.PageAsync("feature", 1, "rating", "asc");

        // Assert
        Assert.Null(result.feature);
        Assert.IsType<int>(result.current_page);
        Assert.Null(result.photos);
      }
    }
  }
}