﻿using Microsoft.AspNetCore.Mvc;
using PopularPhotos.Web.Controllers;
using Xunit;

namespace PopularPhotos.Web.Tests.Controllers
{
  public class HomeControllerTests
  {
    public class IndexMethod : HomeControllerTests
    {
      [Fact]
      public void ShouldReturnIndexViewResult()
      {
        //Arrange
        var controller = new HomeController();

        //Act
        var response = controller.Index();

        //Assert
        var result = Assert.IsType<ViewResult>(response);
        Assert.Equal("Index", result.ViewName);
      }
    }
  }
}