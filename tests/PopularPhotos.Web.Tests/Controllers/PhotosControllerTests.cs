﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using PopularPhotos.Domain;
using PopularPhotos.Web.Controllers;
using PopularPhotos.Web.Models;
using PopularPhotos.Web.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace PopularPhotos.Web.Tests.Controllers
{
  public class PhotosControllerTests
  {
    public class PageMethod : PhotosControllerTests
    {
      public static IEnumerable<object[]> GetPagedMethodMemberData()
      {
        yield return new object[] { "popular", null, new Photo[0] };
        yield return new object[] { "popular", null, new Photo[0] };
      }

      [Theory]
      [MemberData(nameof(GetPagedMethodMemberData))]
      public async Task ShouldReturnEmptyPhotosPagedResponse(
        string feature,
        int? page,
        Photo[] photos)
      {
        //Arrange
        int currentPage = page.HasValue ? page.Value + 1 : 1;
        var photoService = new Mock<IPhotoService>();
        photoService
          .Setup(p => p.PageAsync(feature, page, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
          .ReturnsAsync(new PhotosPagedResponse() {
            feature = feature,
            current_page = currentPage,
            photos = photos
          });

        var controller = new PhotosController(photoService.Object);

        //Act
        var response = await controller.Page("popular", null, string.Empty, string.Empty, string.Empty);

        //Assert
        var result = Assert.IsType<ActionResult<PhotosPagedResponse>>(response);
        var pagedResponse = Assert.IsAssignableFrom<PhotosPagedResponse>(result.Value);
        Assert.Equal(feature, pagedResponse.feature);
        Assert.Equal(currentPage, pagedResponse.current_page);
        Assert.Equal(photos, pagedResponse.photos);
      }
    }
  }
}