﻿using Microsoft.Extensions.Logging;
using Moq;
using PopularPhotos.Web.Clients;
using PopularPhotos.Web.Serializers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace PopularPhotos.Web.Tests.Clients
{
  public class JsonClientTests
  {
    public class GetAsyncMethod : JsonClientTests
    {
      public static IEnumerable<object[]> GetJsonClientData()
      {
        yield return new object[] { "{\"order\":\"asc\", \"page\":1}", new FakeApiResponse() { order = "asc", page = 1 } };
      }

      [Theory]
      [MemberData(nameof(GetJsonClientData))]
      public async Task ShouldReturnValidJsonString(
        string json,
        FakeApiResponse fakeApiResponse)
      {
        //Arrange
        var httpClientHandler = new Mock<IHttpClientHandler>();
        var jsonSerializer = new Mock<IJsonSerializer>();
        
        httpClientHandler
          .Setup(h => h.GetStringAsync(It.IsAny<Uri>()))
          .ReturnsAsync(json);

        jsonSerializer
          .Setup(s => s.Deserialize<FakeApiResponse>(json))
          .Returns(fakeApiResponse);
        
        var jsonClient = new JsonClient(httpClientHandler.Object, jsonSerializer.Object);

        //Act
        var result = await jsonClient.GetAsync<FakeApiResponse>(new Uri("/fakeuri", UriKind.Relative));


        //Assert
        Assert.IsType<FakeApiResponse>(result);
        Assert.Equal(fakeApiResponse.order, result.order);
        Assert.Equal(fakeApiResponse.page, result.page);
      }


      [Fact]
      public async Task ShouldThrowArgumentExceptionForAbsoluteUrl()
      {
        //Arrange
        var httpClientHandler = new Mock<IHttpClientHandler>();
        var jsonSerializer = new Mock<IJsonSerializer>();
        var jsonClient = new JsonClient(httpClientHandler.Object, jsonSerializer.Object);

        //Assert
        await Assert.ThrowsAsync<ArgumentException>(() => jsonClient.GetAsync<FakeApiResponse>(new Uri("http://500px.com")));
      }
    }

    public class FakeApiResponse
    {
      public string order { get; set; }
      public int page { get; set; }
    }
  }
}