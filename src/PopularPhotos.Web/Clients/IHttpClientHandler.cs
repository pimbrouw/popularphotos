﻿using System;
using System.Threading.Tasks;

namespace PopularPhotos.Web.Clients
{
  public interface IHttpClientHandler
  {
    Task<string> GetStringAsync(Uri requestUri);
  }
}