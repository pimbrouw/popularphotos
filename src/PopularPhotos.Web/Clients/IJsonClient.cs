﻿using System;
using System.Threading.Tasks;

namespace PopularPhotos.Web.Clients
{
  /// <summary>
  /// IJsonClient represents the ability to broker http requests supportig content-type: application/json
  /// </summary>
  public interface IJsonClient
  {
    /// <summary>
    /// HTTP GET
    /// </summary>
    /// <param name="requestUri"></param>
    /// <returns></returns>
    Task<T> GetAsync<T>(Uri requestUri);
  }
}