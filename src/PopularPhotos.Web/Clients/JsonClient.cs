﻿using PopularPhotos.Web.Serializers;
using System;
using System.Threading.Tasks;

namespace PopularPhotos.Web.Clients
{
  /// <summary>
  /// JsonClient implements IJsonClient, provide access to 500px API
  /// </summary>
  public class JsonClient : IJsonClient
  {
    private readonly IHttpClientHandler httpClientHandler;
    private readonly IJsonSerializer jsonSerializer;

    public JsonClient(
      IHttpClientHandler httpClientHandler,
      IJsonSerializer jsonSerializer)
    {
      this.httpClientHandler = httpClientHandler ?? throw new ArgumentNullException(nameof(httpClientHandler));
      this.jsonSerializer = jsonSerializer ?? throw new ArgumentNullException(nameof(jsonSerializer));
    }

    /// <summary>
    /// HTTP GET
    /// </summary>
    /// <param name="requestUri"></param>
    /// <returns></returns>
    public async Task<T> GetAsync<T>(Uri requestUri)
    {      
      if(Uri.TryCreate(requestUri.OriginalString, UriKind.Absolute, out Uri absUri))
      {
        throw new ArgumentException(nameof(requestUri));
      }

      var responseBody = await httpClientHandler.GetStringAsync(requestUri);

      if (!string.IsNullOrWhiteSpace(responseBody))
      {
        return jsonSerializer.Deserialize<T>(responseBody);
      }

      return default(T);
    }
  }
}