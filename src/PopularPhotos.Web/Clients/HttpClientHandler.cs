﻿using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace PopularPhotos.Web.Clients
{
  public class HttpClientHandler : IHttpClientHandler
  {
    private const string API_PROIVDER = "500px";
    private readonly HttpClient httpClient;
    private readonly ILogger logger;

    public HttpClientHandler(
      IHttpClientFactory httpClientFactory,
      ILogger<HttpClientHandler> logger)
    {
      if (httpClientFactory == null)
      {
        throw new ArgumentNullException(nameof(httpClientFactory));
      }

      httpClient = httpClientFactory.CreateClient(API_PROIVDER);
      this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    public async Task<string> GetStringAsync(Uri requestUri)
    {
      using (httpClient)
      {
        try
        {
          return await httpClient.GetStringAsync(requestUri);
        }
        catch (Exception ex)
        {
          logger.LogError(ex, "Request failed for: {requestUri}", requestUri);
        }     
      }

      return null;
    }
  }
}