﻿var ko = require('knockout');

/*
 * Custom Bindings
 */
require('./bindings');

/*
 * Global Component Registration
 */
ko.components.register('dropdown-menu', require('./components/dropdown-menu/dropdown-menu'));

/*
 * Photo Component Register
 */
ko.components.register('photo-grid', require('./components/photo-grid/photo-grid'));
ko.components.register('photo-viewer', require('./components/photo-viewer/photo-viewer'));
ko.components.register('photo-lightbox', require('./components/photo-lightbox/photo-lightbox'));

/*
 * Initialize application
 */
ko.applyBindings({});