﻿var
  ko = require('knockout'),
  template = require('fs').readFileSync(__dirname + '/dropdown-menu.html', 'utf8').replace(/^\uFEFF/, ''),
  SelectListItem = require('../../models/select-list-item');

module.exports = {
  viewModel: DropdownMenu,
  template: template
};

/*
 * DropdownMenu renders a configurable dropdown-menu list
 */
function DropdownMenu(params) {
  var self = this;

  /*
   * State
   */
  self.state = {
    // Selected item
    item: ko.observable(),

    // Selected item label
    label: ko.pureComputed(function () {
      var label = self.data.defaultLabel;
      var item = ko.unwrap(self.state.item);

      if (item && self.data.itemLabel) {
        label = item[self.data.itemLabel];
      }
      
      return label; 
    })
  };

  /*
   * Data
   */
  self.data = {
    // Default dropdown text
    defaultLabel: 'Choose...',

    // Property of item to display when chosen (Default to 'label' for easy SelectListItem integration)
    itemLabel: 'label',

    // Items to make selectable
    items: []
  };

  /*
   * Actions
   */
  self.actions = {
    setItem: function (item) {      
      if (item !== self.state.item.peek()) {
        self.state.item(item);
      }
    }
  };

  /*
   * Initializer
   */
  self.init(params);
}

/*
 * Component Initializer 
 */
DropdownMenu.prototype.init = function (params) {
  var self = this;

  // Selected item (note use of equals to prevent wrapping observable with observable)
  if (ko.isObservable(params.value)) self.state.item = params.value;

  // Dropdown items
  self.data.items = params.items;

  // Default label
  if (params.defaultLabel) self.data.defaultLabel = params.defaultLabel;

  // Item label
  if (params.itemLabel) self.data.itemLabel = params.itemLabel;
};