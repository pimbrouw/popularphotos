﻿var
  ko = require('knockout'),
  template = require('fs').readFileSync(__dirname + '/photo-viewer.html', 'utf8').replace(/^\uFEFF/, '');

module.exports = {
  viewModel: PhotoViewer,
  template: template
};

/*
 * PhotoViewer renders an image thumbnail with ability to view full-res on click
 */
function PhotoViewer(params) {
  var self = this;

  /*
   * State
   */
  self.state = {
    lightbox: ko.observable()
  };

  /*
   * Data
   */
  self.data = {
    // Full res photo configureable key
    fullResId: 5,

    // Full res photo resolver
    fullResUrl: ko.pureComputed(function () {
      if (self.data && self.data.photo && self.data.photo.images) {

        for (var i = 0; i < self.data.photo.images.length; i++) {
          var image = self.data.photo.images[i];

          if (image.size === self.data.fullResId) {
            return image.url;
          }
        }
      }

      return null;
    }),

    // Thumbnail photo configurable key
    thumbnailId: 600,

    // Thumbnail resolver
    thumbnailUrl: ko.pureComputed(function () {         
      if (self.data && self.data.photo && self.data.photo.images) {
        
        for (var i = 0; i < self.data.photo.images.length; i++) {
          var image = self.data.photo.images[i];
          
          if (image.size === self.data.thumbnailId) {
            return image.url;
          }
        }
      }

      return null;
    }),

    // Photo details
    photo: null   
  };

  /*
   * Actions
   */
  self.actions = {
    toggleLightbox: function () {
      self.state.lightbox(!self.state.lightbox.peek());
    }
  };

  /*
   * Initialize
   */
  self.init(params);
}

/*
 * Component Initializer
 */
PhotoViewer.prototype.init = function (params) {
  var self = this;

  // Fullres image key
  if (params.fullResId) self.data.fullResId = params.fullResId;

  // Thumbnail image key
  if (params.thumbnailId) self.data.thumbnailId = params.thumbnailId;

  // Photo
  self.data.photo = params.photo;
};