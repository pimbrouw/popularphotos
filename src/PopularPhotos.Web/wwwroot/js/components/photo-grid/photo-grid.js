﻿var
  ko = require('knockout'),
  template = require('fs').readFileSync(__dirname + '/photo-grid.html', 'utf8').replace(/^\uFEFF/, ''),
  photoRepository = require('../../repositories/photo'),
  SelectListItem = require('../../models/select-list-item');

module.exports = {
  viewModel: PhotoGrid,
  template: template
};

/*
 * PhotoGrid renders grid layout of PhotoViewer components
 */
function PhotoGrid(params) {
  var self = this;

  /*
   * State
   */
  self.state = {
    // General error state
    error: ko.observable(false),

    // Loading photos
    loading: ko.observable(true),

    // Efficient check for photo existence
    hasPhotos: ko.pureComputed(function () {
      var photos = ko.unwrap(self.data.photos);

      return photos.length;
    }),

    // Selected values
    feature: ko.observable(),    
    order: ko.observable(),
    page: ko.observable(1),
    sort: ko.observable(),    
  };

  /*
   * Data
   */
  self.data = {
    // 500px image sizes to request
    image_size: [600, 5],   

    // Max page allowed
    maxPage: 1,

    // Filter collections
    features: ko.observableArray(),    
    orders: ko.observableArray(),
    photos: ko.observableArray(),
    sorts: ko.observableArray(),
  };

  /*
   * Actions
   */
  self.actions = {
    // Execute a load more photos request
    loadMore: function () {
      var page = ko.unwrap(self.state.page) || 1;

      // If we haven't exceeded the last page, increment and trigger load
      if (page < self.data.maxPage) {
        self.state.page(page + 1);
      }
    }
  };

  /*
   * Initializer
   */
  self.init();
}

/*
 * Component Initializer
 */
PhotoGrid.prototype.init = function () {
  var self = this;

  // Register our subscriptions on form inputs and load more button
  function getNewPhotos() {
    self.data.photos([]);
    self.getPhotos();
  }

  self.state.feature.subscribe(getNewPhotos);
  self.state.sort.subscribe(getNewPhotos);
  self.state.order.subscribe(getNewPhotos);
  self.state.page.subscribe(function () {
    self.getPhotos();
  });

  // Default feature options
  self.data.features([
    new SelectListItem('popular', 'Popular'),
    new SelectListItem('highest_rated', 'Highest Rated'),
    new SelectListItem('upcoming', 'Upcoming'),
    new SelectListItem('fresh_today', 'Fresh Today'),
    new SelectListItem('fresh_yesterday', 'Fresh Yesterday'),
    new SelectListItem('fresh_week', 'Fresh Week')
  ]);

  // Default sort options
  self.data.sorts([
    new SelectListItem('created_at', 'Created At'),
    new SelectListItem('rating', 'Rating'),
    new SelectListItem('highest_rating', 'Highest Rating'),    
  ]);

  // Default order options
  self.data.orders([
    new SelectListItem('asc', 'Ascending'),
    new SelectListItem('desc', 'Descending')
  ]);

  self.getPhotos();
};

/*
 * GET /api/photos 
 */
PhotoGrid.prototype.getPhotos = function (pre, success, error, always) {
  var self = this;

  self.state.loading(true);

  if (pre) pre();

  var feature = ko.unwrap(self.state.feature);
  var page = ko.unwrap(self.state.page);
  var sort = ko.unwrap(self.state.sort);
  var order = ko.unwrap(self.state.order);
  
  photoRepository.page({
    feature: feature ? feature.value : null,
    image_size: self.data.image_size,
    page: page ? page : null,
    sort: sort ? sort.value : null,
    order: order ? order.value : null
  },
    function (resp) {

      if (resp.total_pages)
        self.data.maxPage = resp.total_pages;

      if(resp.photos)
        ko.utils.arrayPushAll(self.data.photos, resp.photos);

      if (success) success();
    },
    function (err) {
      console.warn(err);
      self.state.error(true);

      if (error) error();
    },
    function () {
      self.state.loading(false);

      if (always) always();
    });
}