﻿var
  ko = require('knockout'),
  template = require('fs').readFileSync(__dirname + '/photo-lightbox.html', 'utf8').replace(/^\uFEFF/, '');

module.exports = {
  viewModel: PhotoLightbox,
  template: template
};

/*
 * PhotoLightbox renders an image lightbox with ability to display some image details
 */
function PhotoLightbox(params) {
  var self = this;

  /*
   * Data
   */
  self.data = {    
    // Image full res url
    url: null,

    // Descriptor contains: name, description and external link
    descriptor: null
  };

  /*
   * Actions
   */
  self.actions = {
    // Configurable function to hide lightbox (empty function to prevent runtime error)
    hide: function () { }
  };

  /*
   * Initialize
   */
  self.init(params);
}

/*
 * Component Initializer
 */
PhotoLightbox.prototype.init = function (params) {
  var self = this;

  // Full res image url
  self.data.url = params.url;

  // Hide lightbox callback
  self.actions.hide = params.hide;

  // Descriptor
  if (params.name ||
    params.description ||
    params.external) {
    self.data.descriptor = {};
  }

  if (params.name) self.data.descriptor.name = params.name;

  if (params.description) self.data.descriptor.description = params.description;

  if (params.external) self.data.descriptor.external = params.external;
};