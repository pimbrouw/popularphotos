﻿var ko = require('knockout');

ko.bindingHandlers.backgroundImage = {
  update: function (element, valueAccessor) {
    // Unwrap url safely from binding
    var url = ko.unwrap(valueAccessor());

    // Apply url to element style
    ko.bindingHandlers.style.update(element,
      function () { return { backgroundImage: "url('" + url + "')" } });
  }
};
