﻿module.exports = SelectListItem;

/**
 * Used to denote simple select list item with a "value" and "label".
 * Referenced in:
 * - photo-grid
 * - dropdown-menu
 * 
 * @typedef {object} SelectListItem
 * @param {string} value the alphanumeric value representing the item
 * @param {string} label the text to display when showing the item
 */
function SelectListItem(value, label) {
  this.value = value;
  this.label = label;
}