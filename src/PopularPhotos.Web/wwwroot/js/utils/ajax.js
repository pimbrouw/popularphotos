﻿module.exports = new Ajax();

/**
 * Ajax request helper
 * @typedef {object} Ajax
 */
function Ajax() { }

/**
 * GET request helper
 * @param {any} url url
 * @param {any} success success calback
 * @param {any} error error callback
 * @param {any} always always callback
 */
Ajax.prototype.get = function (url, success, error, always) {
  var req = this.makeRequest('GET', url, success, error, always);
  req.send();
};

Ajax.prototype.makeRequest = function (method, url, success, error, always) {
  var req = new XMLHttpRequest();
  req.open(method, url, true);

  req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

  req.onreadystatechange = function () {
    if (req.readyState === 4) {
      if (req.status >= 200 && req.status < 400) {
        success(req.responseText);
      }
      else if (error) {
        error(req.statusText);
      }

      if (always) always();
    }
  };

  req.onerror = function () {
    if (error) error();
  };

  return req;
};  