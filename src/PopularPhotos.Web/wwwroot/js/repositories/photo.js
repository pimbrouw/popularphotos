﻿var
  ko = require('knockout'),
  ajax = require('../utils/ajax');

module.exports = new PhotoRepository();

/**
 * Enscapsulates /api/photos requests.
 * @typedef {object} PhotoRepository
 */
function PhotoRepository() {  
}

/**
 * paginate requests to /api/photos
 * @param {any} options request options (feature, current_page, sort, order)
 * @param {any} success success callback
 * @param {any} error error callback
 * @param {any} always always callback
 */
PhotoRepository.prototype.page = function (options, success, error, always) {
  var queryString = "?image_size=" + options.image_size.toString() + "&";
  
  if (options.feature) queryString += "feature=" + options.feature + "&";
  if (options.page) queryString += "page=" + options.page + "&";
  if (options.sort) queryString += "sort=" + options.sort + "&";
  if (options.order) queryString += "order=" + options.order + "&";

  ajax.get('/api/photos' + queryString, function (respText) {
    success(ko.utils.parseJson(respText));
  }, error, always);
}