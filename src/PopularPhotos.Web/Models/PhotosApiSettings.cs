﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PopularPhotos.Web.Models
{
  public class PhotosApiSettings
  {
    public string Url { get; set; }
    public string Key { get; set; }
  }
}
