﻿using PopularPhotos.Domain;
using System.Collections.Generic;

namespace PopularPhotos.Web.Models
{
  public class PhotosPagedResponse : PagedResponse
  {
    public string feature { get; set; }
    public Photo[] photos { get; set; }
  }
}