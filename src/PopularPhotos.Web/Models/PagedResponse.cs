﻿namespace PopularPhotos.Web.Models
{
  public abstract class PagedResponse
  {
    public int current_page { get; set; }
    public int total_pages { get; set; }
    public int total_items { get; set; }
  }
}