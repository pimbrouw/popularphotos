﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace PopularPhotos.Web.Serializers
{
  public class JsonSerializer : IJsonSerializer
  {
    public T Deserialize<T>(string input)
    {
      if (string.IsNullOrWhiteSpace(input))
      {
        throw new ArgumentNullException("input cannot be null or empty");
      }

      return JsonConvert.DeserializeObject<T>(input);
    }

    public string Serialize(object obj)
    {
      if (obj == null)
      {
        throw new ArgumentNullException("obj cannot be null");
      }

      return JsonConvert.SerializeObject(obj);
    }
  }
}