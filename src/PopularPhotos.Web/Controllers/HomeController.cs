﻿using Microsoft.AspNetCore.Mvc;

namespace PopularPhotos.Web.Controllers
{
  public class HomeController : Controller
  {
    [HttpGet]
    public IActionResult Index()
    {
      return View("Index");
    }
  }
}