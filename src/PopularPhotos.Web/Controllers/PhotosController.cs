﻿using Microsoft.AspNetCore.Mvc;
using PopularPhotos.Web.Models;
using PopularPhotos.Web.Services;
using System;
using System.Threading.Tasks;

namespace PopularPhotos.Web.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class PhotosController : Controller
  {
    private readonly IPhotoService photoService;

    public PhotosController(
      IPhotoService photoService)
    {
      this.photoService = photoService ?? throw new ArgumentNullException(nameof(photoService));
    }

    [HttpGet]
    public async Task<ActionResult<PhotosPagedResponse>> Page(
      string feature,
      int? page,
      string sort,
      string order,
      string image_size)
    {
      return await photoService.PageAsync(feature, page, sort, order, image_size);
    }
  }
}