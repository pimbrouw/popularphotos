﻿using PopularPhotos.Web.Clients;
using PopularPhotos.Web.Models;
using PopularPhotos.Web.Serializers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace PopularPhotos.Web.Services
{
  /// <summary>
  /// PhotoService implements IPhotoService and exposes 500px photo data via:
  /// - PageAsync: paginated Photo entity response
  /// </summary>
  public class PhotoService : IPhotoService
  {

    private readonly IJsonClient jsonClient;
    private readonly PhotosApiSettings photosApiSettings;

    public PhotoService(
      IJsonClient jsonClient,
      PhotosApiSettings photosApiSettings)
    {
      this.jsonClient = jsonClient ?? throw new ArgumentNullException(nameof(jsonClient));
      this.photosApiSettings = photosApiSettings ?? throw new ArgumentNullException(nameof(photosApiSettings));
    }

    /// <summary>
    /// Represents supported features
    /// </summary>
    private string[] supportedFeatures =>
      new string[] { "popular", "highest_rated", "upcoming", "editors", "fresh_today", "fresh_yesterday", "fresh_week" };

    /// <summary>
    /// Represents support sort fields
    /// </summary>
    private string[] supportedSorts =>
      new string[] { "created_at", "rating", "highest_rating" };

    /// <summary>
    /// PageAsync implementation to encapsulate external 500px API request
    /// GET /v1/photos
    /// </summary>
    /// <param name="feature"></param>
    /// <param name="page"></param>
    /// <param name="sort"></param>
    /// <param name="order"></param>
    /// <returns></returns>
    public async Task<PhotosPagedResponse> PageAsync(
      string feature,
      int? page,
      string sort,
      string order,
      string imageSize = null)
    {
      var queryParameters = new HashSet<string>();

      // Append consumer key
      AppendQueryParameter(queryParameters, "consumer_key", photosApiSettings.Key);

      // Append image size
      AppendQueryParameter(queryParameters, "image_size", imageSize);

      // Append page
      AppendQueryParameter(queryParameters, "page", page.ToString(), _ => page.HasValue);

      // Parse, validate and append feature parameter
      AppendQueryParameter(queryParameters, "feature", feature, (f) =>
      {
        return !string.IsNullOrWhiteSpace(f) &&
        Array.IndexOf(supportedFeatures, f) > -1;
      });

      // Parse, validate and append sort parameter
      AppendQueryParameter(queryParameters, "sort", sort, (s) =>
      {
        return !string.IsNullOrWhiteSpace(s) &&
        Array.IndexOf(supportedSorts, s) > -1;
      });

      // Parse, validate and append sort parameter
      AppendQueryParameter(queryParameters, "sort_direction", order, (o) =>
      {
        return string.Equals("asc", o, StringComparison.OrdinalIgnoreCase) ||
        string.Equals("desc", o, StringComparison.OrdinalIgnoreCase);
      });

      // Make request uri, concatenated with query string
      var queryString = string.Join("&", queryParameters);
      var requestUri = new Uri("photos?" + queryString, UriKind.Relative);

      // Execute request
      return (await jsonClient.GetAsync<PhotosPagedResponse>(requestUri)) ?? new PhotosPagedResponse();      
    }

    private void AppendQueryParameter(
      ISet<string> queryParameters,
      string key,
      string value,
      Func<string, bool> predicate = null)
    {
      if (predicate == null || predicate(value))
      {
        queryParameters.Add(key + "=" + value);
      }
    }
  }
}