﻿using PopularPhotos.Web.Models;
using System.Threading.Tasks;

namespace PopularPhotos.Web.Services
{
  public interface IPhotoService
  {
    /// <summary>
    /// Page returns Photo entities contained in an API PagedResponsed
    /// Enables filtering on: feature
    /// Enables pagination by: page, sort, order (asc OR desc)
    /// </summary>
    /// <param name="feature"></param>
    /// <param name="page"></param>
    /// <param name="sort"></param>
    /// <param name="order"></param>
    /// <param name="image_size"></param>
    /// <returns></returns>
    Task<PhotosPagedResponse> PageAsync(
      string feature,
      int? page,
      string sort,
      string order,
      string image_size = null);
  }
}