﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PopularPhotos.Web.Clients;
using PopularPhotos.Web.Models;
using PopularPhotos.Web.Serializers;
using PopularPhotos.Web.Services;
using Serilog;
using System;
using System.Net.Http;

namespace PopularPhotos.Web
{
  public class Startup
  {
    public Startup(IHostingEnvironment env)
    {
      // Load global and local configuration values
      var configBuilder = new ConfigurationBuilder()
        .SetBasePath(env.ContentRootPath)
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddJsonFile("appsettings.local.json", optional: false, reloadOnChange: true)
        .AddEnvironmentVariables();

      Configuration = configBuilder.Build();

      //
    }

    /// <summary>
    /// IConfiguration is a key/value store for global config variables
    /// </summary>
    public IConfiguration Configuration { get; }

    /// <summary>
    /// ConfigureServices is responsible for registering dependencies
    /// </summary>
    /// <param name="services"></param>
    public void ConfigureServices(IServiceCollection services)
    {            
      EnableLogging(services);
      EnableJson(services);
      EnableHttpServices(services);
      EnableMvc(services);
      EnableApiServices(services);
    }

    /// <summary>
    /// Configure is responsible for startup-time configuration of dependencies
    /// </summary>
    /// <param name="app"></param>
    /// <param name="env"></param>
    public void Configure(
      IApplicationBuilder app,
      IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseStaticFiles();

      app.UseMvc(BuildRoutes);
    }

    private void BuildRoutes(IRouteBuilder routeBuilder)
    {
      routeBuilder.MapRoute(
        "default",
        "{controller}/{action}/{id?}",
        new { Controller = "Home", Action = "Index" });
    }

    private void EnableApiServices(IServiceCollection services)
    {
      // Register application settings as dependency
      services.Configure<PhotosApiSettings>(Configuration.GetSection("PhotosApiSettings"));
      
      // Register individual API Services
      services.AddScoped<IPhotoService, PhotoService>(ctx =>
      {
        var jsonClient = ctx.GetRequiredService<IJsonClient>();
        var photosApiSettings = ctx.GetRequiredService<IOptions<PhotosApiSettings>>();

        // Throw fatal on missing API key OR url
        if (string.IsNullOrWhiteSpace(photosApiSettings?.Value?.Key))
        {
          throw new ArgumentNullException("PhotosApiSettings.Key");
        }
        else if (string.IsNullOrWhiteSpace(photosApiSettings?.Value?.Url))
        {
          throw new ArgumentNullException("PhotosApisettings.Url");
        }

        return new PhotoService(jsonClient, photosApiSettings.Value);
      });
    }

    private void EnableHttpServices(IServiceCollection services)
    {
      // Named HTTP Clients
      services.AddHttpClient("500px", (options, client) =>
      {
        var photoApiSettings = options.GetRequiredService<IOptions<PhotosApiSettings>>().Value;

        client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");

        client.BaseAddress = new Uri(photoApiSettings.Url);
      });

      // HTTP Client Handler
      services.AddScoped<IHttpClientHandler, Clients.HttpClientHandler>();
    }

    private void EnableJson(IServiceCollection services)
    {
      // Register JSON support services
      services.AddTransient<IJsonSerializer, JsonSerializer>();
      services.AddTransient<IJsonClient, JsonClient>();
    }

    private void EnableLogging(IServiceCollection services)
    {
      // Enable logging with output to rolling file retained for 30 days
      Log.Logger = new LoggerConfiguration()
        .MinimumLevel.Is(Serilog.Events.LogEventLevel.Warning)
        .WriteTo.RollingFile("logs/log-{Date}.log", retainedFileCountLimit: 30)
        .CreateLogger();

      // Add logging dependency, garbage collecting when host provider is collected
      services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));
    }

    private void EnableMvc(IServiceCollection services)
    {
      // Enable MVC using only the startup features we need
      services
        .AddMvcCore()
        .AddRazorViewEngine()
        .AddJsonFormatters();
    }
  }
}