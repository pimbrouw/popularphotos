﻿using System;

namespace PopularPhotos.Domain
{
  /// <summary>
  /// Photo represents a 500px "Photo" Entity
  /// </summary>
  public class Photo
  {
    public int id { get; set; }
    public string name { get; set; }
    public string description { get; set; }    
    public ImageUrl[] images { get; set; }
    public string[] image_url { get; set; }
    public string url { get; set; }

    public User user { get; set; }

    public string external_url => "https://500px.com" + url;
  }
}