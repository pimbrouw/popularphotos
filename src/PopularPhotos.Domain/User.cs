﻿namespace PopularPhotos.Domain
{
  /// <summary>
  /// User represents a 500px "User" entity
  /// </summary>
  public class User
  {
    public int id { get; set; }
    public string username { get; set; }    
    public string fullname { get; set; }    
  }
}