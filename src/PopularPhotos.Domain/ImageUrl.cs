﻿namespace PopularPhotos.Domain
{
  public class ImageUrl
  {
    public string url { get; set; }
    public int size { get; set; }    
  }
}
